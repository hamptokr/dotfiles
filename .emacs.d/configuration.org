#+TITLE: Emacs Configuration
#+AUTHOR: Kramer Hampton
#+EMAIL: khampton@estanalytical.com

* Initialization
Make sure =use-package= will install the package if it's not already available.
#+BEGIN_SRC emacs-lisp
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)
#+END_SRC
Always compile packages and use the newest version available.
#+BEGIN_SRC emacs-lisp
(use-package auto-compile
  :config (auto-compile-on-load-mode))
(setq load-prefer-newer t)
#+END_SRC
This will setup environment variables and settings for Custom.
#+BEGIN_SRC emacs-lisp
(use-package exec-path-from-shell)
(when (memq window-system '(mac ns x))
  (exec-path-from-shell-copy-env "RUST_SRC_PATH")
  (exec-path-from-shell-initialize))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("617341f1be9e584692e4f01821716a0b6326baaec1749e15d88f6cc11c288ec6" default)))
 '(package-selected-packages
   (quote
    (ssh-agency zenburn-theme exec-path-from-shell company racer dracula-theme magit rust-mode))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
#+END_SRC

* Theme
#+BEGIN_SRC emacs-lisp
(use-package monokai-pro-theme :ensure t :config (load-theme 'monokai-pro t))
#+END_SRC

* Emacs Settings
Turn on line numbering.
#+BEGIN_SRC emacs-lisp
(global-linum-mode 1)
#+END_SRC
Auto close the follwing characters ="'[{(=
#+BEGIN_SRC emacs-lisp
(electric-pair-mode 1)
#+END_SRC
Stop emacs from creating backups in same folder
#+BEGIN_SRC emacs-lisp
(setq backup-directory-alist `(("." . "~/.emacs_saves")))
(setq backup-by-copying t)
#+END_SRC
Font settings
#+BEGIN_SRC emacs-lisp
(add-to-list 'default-frame-alist '(font . "Operator Mono Medium-12"))
#+END_SRC
Set bigger frame size
#+BEGIN_SRC emacs-lisp
(add-to-list 'default-frame-alist '(height . 48))
(add-to-list 'default-frame-alist '(width . 160))
#+END_SRC
Highlight TODOs, FIXMEs, BUGs, NOTEs
Note: Thanks Casey Muratori!
#+BEGIN_SRC emacs-lisp
(setq fixme-modes '(rust-mode c++-mode c-mode))
(make-face 'font-lock-fixme-face)
(make-face 'font-lock-note-face)
(mapc (lambda (mode)
        (font-lock-add-keywords
         mode
         '(("\\<\\(TODO\\)" 1 'font-lock-fixme-face t)
           ("\\<\\(BUG\\)"  1 'font-lock-fixme-face t)
           ("\\<\\(NOTE\\)" 1 'font-lock-note-face t))))
       fixme-modes)
(modify-face 'font-lock-fixme-face "Red" nil nil t nil t nil nil)
(modify-face 'font-lock-note-face "Dark Green" nil nil t nil t nil nil)
#+END_SRC
Add htmlize support for code blocks
#+BEGIN_SRC emacs-lisp
(use-package htmlize)
#+END_SRC
Set Default Working Directory
#+BEGIN_SRC emacs-lisp
(setq inhibit-startup-message t)
(setq default-directory "~/")
#+END_SRC
* Magit
Set the global hotkey for magit.
#+BEGIN_SRC emacs-lisp
(use-package magit)
(global-set-key (kbd "C-x g") 'magit-status)
#+END_SRC
Prompt for ssh password if it isn't authenticated.
#+BEGIN_SRC emacs-lisp
(setenv "SSH_ASKPASS" "git-gui--askpass")
#+END_SRC

* Language Mode Settings
#+BEGIN_SRC emacs-lisp
(use-package company)
(add-hook 'after-init-hook 'global-company-mode)
(use-package flycheck :ensure t :init (global-flycheck-mode))
#+END_SRC
** rust-mode
Add hooks for =racer=, =eldoc=, and =company=
#+BEGIN_SRC emacs-lisp
(use-package rust-mode)
(use-package racer)
(add-hook 'rust-mode-hook #'racer-mode)
(add-hook 'racer-mode-hook #'eldoc-mode)
(add-hook 'racer-mode-hook #'company-mode)
#+END_SRC
Setup =rust-mode= and tab completion
#+BEGIN_SRC emacs-lisp
(define-key rust-mode-map (kbd "TAB") #'company-indent-or-complete-common)
(setq company-tooltip-align-annotations t)
#+END_SRC

Flychecking Rust
#+BEGIN_SRC emacs-lisp
(use-package flycheck-rust)
(with-eval-after-load 'rust-mode
  (add-hook 'flycheck-mode-hook #'flycheck-rust-setup))
#+END_SRC

** python-mode
python-mode specific settings, for jedi you must run:
=M-x jedi:install-server= to complete installation
#+BEGIN_SRC emacs-lisp
(use-package jedi)
(add-hook 'python-mode-hook 'jedi:setup)
(setq jedi:complete-on-dot t)
#+END_SRC
** cpp
cmake-ide setup
#+BEGIN_SRC emacs-lisp
(use-package levenshtein)
(use-package cmake-ide)
(cmake-ide-setup)
#+END_SRC

* org-mode
Global hotkey setup for org-mode
#+BEGIN_SRC emacs-lisp
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-cb" 'org-switchb)
#+END_SRC
Add custom todo states and add time when done-d
#+BEGIN_SRC emacs-lisp
(setq org-todo-keywords
      '((sequence "TODO(t)" "FEEDBACK(f)" "VERIFY(v)" "|" "DONE(d)")
	(sequence "REPORTED(r)" "BUG(b)" "KNOWN-CAUSE(k)" "|" "WONT-FIX(w)" "FIXED(f)")))
(setq org-log-done 'time)
#+END_SRC

